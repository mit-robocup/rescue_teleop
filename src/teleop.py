#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Joy
from roboteq_msgs.msg import Command

TURN_RATE = 95


def arcadeAlgo(x, y):
    x = (x*TURN_RATE);
    left = max(min((y - x), 1),-1)
    right = max(min((y + x), 1),-1)
    return (left,right)

# Receives joystick messages (subscribed to Joy topic)
# then converts the joysick inputs into Twist commands
# axis 1 aka left stick vertical controls linear speed
# axis 0 aka left stick horizonal controls angular speed
def callback(data):

    stick = (data.axes[3], data.axes[4])


    twist = Twist()
    twist.linear.x = 4*data.axes[1]
    twist.angular.z = 4*data.axes[0]
    fwdGain=10*(data.axes[3]+1)
    turnGain=fwdGain*.8
    leftCommand = Command()
    leftCommand.mode = 0
    leftCommand.setpoint = -1*(fwdGain*twist.linear.x-turnGain*twist.angular.z)
    rightCommand = Command()
    rightCommand.mode = 0
    rightCommand.setpoint = fwdGain*twist.linear.x+turnGain*twist.angular.z

    pubLeft.publish(leftCommand)
    pubRight.publish(rightCommand)

    pub.publish(twist)

# Intializes everything
def start():
    # publishing to "turtle1/cmd_vel" to control turtle1
    global pubRight
    global pubLeft

    pubRight = rospy.Publisher('dt/right/cmd',Command)
    pubLeft = rospy.Publisher('dt/left/cmd',Command)
    # subscribed to joystick inputs on topic "joy"
    rospy.Subscriber("joy", Joy, callback)
    # starts the node
    rospy.init_node('joy_teleop')
    rospy.spin()

if __name__ == '__main__':
    start()
