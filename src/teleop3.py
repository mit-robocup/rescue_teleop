#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Joy
from roboteq_msgs.msg import Command

TURN_RATE = 0.55
VELOCITY_SCALE = ((1.0/5.45)*(2*0.12*3.14)*60) * 0.5


def arcadeAlgo(x, y, mult=1.0):
    x *= -TURN_RATE;
    left = max(min((y - x), 1.0),-1.0)*VELOCITY_SCALE*mult
    right = max(min((y + x), 1.0),-1.0)*VELOCITY_SCALE*mult
    return (-right, left)

# Receives joystick messages (subscribed to Joy topic)
# then converts the joysick inputs into Twist commands
# axis 1 aka left stick vertical controls linear speed
# axis 0 aka left stick horizonal controls angular speed
def callback(data):
    #Chose axes to use here
    stick = (data.axes[0], data.axes[1])

    #Compute left and right cmd\
    oup = arcadeAlgo(stick[0],stick[1])

    leftCommand = Command()
    leftCommand.mode = 0
    leftCommand.setpoint = oup[0]

    rightCommand = Command()
    rightCommand.mode = 0
    rightCommand.setpoint = oup[1]

    pubLeft.publish(leftCommand)
    pubRight.publish(rightCommand)

# Intializes everything
def start():
    # publishing to "turtle1/cmd_vel" to control turtle1
    global pubRight
    global pubLeft

    pubRight = rospy.Publisher('dt/right/cmd',Command, queue_size=10)
    pubLeft = rospy.Publisher('dt/left/cmd',Command, queue_size=10)
    
    # starts the node
    rospy.init_node('joy_teleop')
    rospy.Subscriber("joy", Joy, callback)
    rospy.spin()

if __name__ == '__main__':
    start()
